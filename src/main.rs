#[derive(Debug)]
enum Expr {
    Val(i32),
    Div(Box<Expr>, Box<Expr>),
}

macro_rules! val {
    ( $e:expr ) => {
        Expr::Val($e)
    };
}

macro_rules! div {
    ( $x:expr, $y:expr ) => {
        Expr::Div(Box::new($x), Box::new($y))
    };
}

fn safediv(x: i32, y: i32) -> Option<i32> {
    if y == 0 {
        return None;
    }
    Some(x / y)
}

fn bind<T>(m: Option<T>, f: Box<dyn FnOnce(T) -> Option<T>>) -> Option<T> {
    match m {
        None => None,
        Some(n) => f(n),
    }
}

macro_rules! bind {
    ( $lhs:expr, $rhs:expr ) => {
        bind($lhs, Box::new($rhs))
    };
}

fn eval(e: Expr) -> Option<i32> {
    match e {
        Expr::Val(i) => Some(i),
        Expr::Div(x, y) => bind!(eval(*x), |n| bind!(eval(*y), move |m| safediv(n, m))),
    }
}

fn main() {
    let i = val!(1);
    let j = div!(val!(6), val!(2));
    let k = div!(val!(6), div!(val!(3), val!(0)));
    println!("i = {:?}", i);
    println!("j = {:?}", j);
    println!("k = {:?}", k);
    println!("eval(i) = {:?}", eval(i));
    println!("eval(j) = {:?}", eval(j));
    println!("eval(k) = {:?}", eval(k));
}
